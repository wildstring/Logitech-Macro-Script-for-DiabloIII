#SingleInstance force ;只能启动一个ahk程序实例，防止重复启动
#Persistent
#NoEnv

;暗黑3/圣教军/阿克汉天谴/AHK脚本
;##################################
;说明：
;-----MacroID = 0代表宏已停止启动，MacroID = 1代表步兵宏已启动，MacroID = 2代表骑兵宏已启动
;-----步兵宏启动后，左键切换天谴延迟800ms，右键切换天谴延迟650ms
;-----骑兵宏启动后，左键停止一切技能上马跑，右键下马开启一切技能砍(同时左键连点拾取)
;-----宏启动后，按住Alt键左键连点拾取
;-----宏启动后，按住Ctrl键取消强制移动，人物原地站立
;-----宏启动后，按键盘H回城并停止宏，再启动按键盘1|2
;默认按键：
;-----鼠标左键 = 阿卡拉特勇士
;-----鼠标右键 = 天谴
;-----键盘A = 挑衅
;-----键盘S = 勇气律法
;-----键盘D = 主要技能/战马冲锋
;-----键盘F = 钢铁之肤
;-----键盘W = 强制移动
;-----键盘1 = 启动/停止步兵宏
;-----键盘2 = 启动/停止骑兵宏
;##################################

;保证宏只在暗黑3中执行
#IfWinActive, ahk_class D3 Main Window Class
	Global MacroID := 0
	Global DefaultDelay := 800

	;圣教军 - 阿克汉天谴(大小秘境步兵)
	;键盘1开启步兵宏
	$1::
		If (MacroID = 0) {
			DiabloIIIMacro(True, True, False, 1)
		} Else If (MacroID = 1) {
			DiabloIIIMacro(False, False, False, 0)
		}
	Return

	;圣教军 - 阿克汉天谴(贤者骑兵)
	;键盘2开启骑兵宏
	$2::
		If (MacroID = 0) {
			DiabloIIIMacro(False, True, False, 2)
			Gosub, 阿卡拉特勇士
			Gosub, 战马冲锋
		} Else If (MacroID = 2) {
			DiabloIIIMacro(False, False, False, 0)
		}
	Return

	;点击左键效果：步兵切换天谴延迟800ms，骑兵是上马跑
	;~修饰符为不屏蔽按键(鼠标左键)的原功能
	~LButton::
		If (MacroID = 1) { ;步兵
			SwitchCondemn(DefaultDelay)
			SoundBeep
		} Else If (MacroID = 2) { ;骑兵
			DiabloIIIMacro(False, True, False)
			Gosub, 阿卡拉特勇士
			Gosub, 战马冲锋
		}
	Return

	;点击右键效果：步兵切换天谴延迟650ms，骑兵是下马砍
	;$修饰符为屏蔽按键(鼠标右键)的原功能
	$RButton::
		If (MacroID = 1) { ;步兵
			SwitchCondemn(600)
			SoundBeep
			SoundBeep
			SoundBeep
		} Else If (MacroID = 2) { ;骑兵
			Gosub, 阿卡拉特勇士
			Gosub, 钢铁之肤
			Gosub, 勇气律法
			Gosub, 天谴
			DiabloIIIMacro(True, True, False) ;
		} Else {
			Gosub, 右键
		}
	Return

	;按住W效果：左键连点，解决强制移动时拾取不便
	~W::
		If (MacroID > 0) {
			While GetKeyState("W", "P") {
				Gosub, 左键
				Sleep 50
			}
		}
	Return

	;大地图、装备、聊天、快速回程等等停止宏，聊胜于无
	~`::
	~B::
	~H::
	~Enter::
	~Space::
	~Q::
		If (MacroID > 0) {
			DiabloIIIMacro(False, False, False, 0)
		}
	Return

	;技能设置：用户自定义修改技能按键配置
	强制移动:
		DiabloIIISendInput("{W}")
	Return

	左键:
		DiabloIIISendInput("{LButton}")
	Return

	右键:
		DiabloIIISendInput("{RButton}")
	Return

	挑衅:
		DiabloIIISendInput("{A}")
	Return

	勇气律法:
		DiabloIIISendInput("{S}")
	Return

	战马冲锋:
		DiabloIIISendInput("{D}")
	Return

	钢铁之肤:
		DiabloIIISendInput("{F}")
	Return

	阿卡拉特勇士:
		DiabloIIISendInput("+{LButton}")
	Return

	天谴:
		DiabloIIISendInput("{RButton}")
	Return

#IfWinActive

;函数：保证按键和点击只在暗黑3中发送
DiabloIIISendInput(Keys) {
    If WinActive("ahk_class D3 Main Window Class") {
    	SendInput % Keys
    }
}

;函数：切换天谴延迟
SwitchCondemn(Delay) {
	SetTimer, 天谴, Off
	SetTimer, 天谴, % Delay
	Gosub, 天谴
}

;函数：宏启动和停止
DiabloIIIMacro(StartMacro, ForceMove, LeftClick, params*) {
	SetTimer, 左键, % (LeftClick ? 50 : "Off")
	SetTimer, 强制移动, % (ForceMove ? 50 : "Off")
	SetTimer, 天谴, % (StartMacro ? DefaultDelay : "Off")
	SetTimer, 阿卡拉特勇士, % (StartMacro ? 250 : "Off")
	SetTimer, 勇气律法, % (StartMacro ? 500 : "Off")
	SetTimer, 钢铁之肤, % (StartMacro ? 500 : "Off")
	If (params[1] <> "") {
		MacroID := params[1]
	}
}
