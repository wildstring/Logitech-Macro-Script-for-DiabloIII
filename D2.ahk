#SingleInstance force
#Persistent
#NoEnv

#IfWinActive, ahk_exe D2R.exe
	Global LeftPotionSlots := 2
	Global RightPotionSlots := 2
	Global CurrentLeftPotionSlot := 0
	Global CurrentRightPotionSlot := 0
	Global PlayerCharacter := 0 ;冰法0、锤丁8、拳丁9
	Global Blizzard_CD := 0 ;暴风雪冷却中
	Global FrozenOrb_Reset := 0 ;连续施放4次冰封球后，重置暴风雪CD (防卡CD)

	;主位移
	~A::
	{
		If (PlayerCharacter = 9) { ;拳丁
			;SendInput {X} ;冲锋+活力
		}
	}
	Return

	;主攻击
	$D::
	{
		If (PlayerCharacter = 0) { ;冰法
			BlizzardMonitor(-2000, True)
		} Else If (PlayerCharacter = 9) { ;拳丁
			;SendInput {R} ;净化
			;SendInput {D} ;天拳
		}
	}
	Return

	;主BUFF
	$Z::
	{
		If (PlayerCharacter = 0) {
			CTABuff("E|J|K|Z|X|E|E", 300, -150000) ;CTA+冰甲+顶球
		} Else {
			SendInput {Z}
		}
	}
	Return

	;一键吨吨左侧N排药水
	$1::
	{
		If (CurrentLeftPotionSlot = 0) {
			CurrentLeftPotionSlot := 0 + LeftPotionSlots
		}
		SendInput % CurrentLeftPotionSlot
		CurrentLeftPotionSlot -= 1
	}
	Return

	;一键吨吨右侧N排药水
	$4::
	{
		If (CurrentRightPotionSlot = 0) {
			CurrentRightPotionSlot := 5 - RightPotionSlots
		}
		SendInput % CurrentRightPotionSlot
		CurrentRightPotionSlot += 1
		If (CurrentRightPotionSlot > 4) {
			CurrentRightPotionSlot := 0
		}
	}
	Return

	;FPS
	$=::
	{
		SendInput {Enter}
		Sleep 50
		SendInput {Raw}/fps
		Sleep 50
		SendInput {Enter}
	}
	Return

	;退出房间(1080P)
	$F1::
	{
		BlockInput On
		SendInput {Esc}
		MouseMove, 960, 475, 0
		SendInput {LButton}
		BlockInput Off
	}
	Return

	;新建地狱房间(1080P)
	$F2::
	{
		BlockInput On
		MouseMove, 800, 970, 0
		SendInput {LButton}
		Sleep 100
		MouseMove, 960, 585, 0
		SendInput {LButton}
		BlockInput Off
	}
	Return

	Blizzard:
		SendInput {D}
		Blizzard_CD := 1
	Return

	Blizzard_ColdDown:
		;SoundBeep, 1000, 50
		Blizzard_CD := 0
	Return

	FrozenOrb:
		SendInput {X}
		FrozenOrb_Num += 1
		If (FrozenOrb_Num > 3) {
			Blizzard_CD := 0 ;重置暴风雪CD
			FrozenOrb_Num := 0
		}
	Return

	BoCDBeep:
		SoundBeep, 300, 700
	Return

#IfWinActive

;函数:暴风雪+冰封球 (暴风雪CD提醒)
BlizzardMonitor(Delay, Follow_FrozenOrb) {
    If (Blizzard_CD = 0) {
    	Gosub, Blizzard
		SetTimer, Blizzard_ColdDown, % Delay
	} Else {
		If (Follow_FrozenOrb) {
			Gosub, FrozenOrb
		}
	}
}

;函数:CTABuff (CD提醒)
CTABuff(Keys, Delay, BoCD) {
	Loop, Parse, % Keys, `|
	{
		SendInput % A_LoopField
		Sleep % Delay
	}
	SetTimer, BoCDBeep, Off
	SetTimer, BoCDBeep, % BoCD
}
