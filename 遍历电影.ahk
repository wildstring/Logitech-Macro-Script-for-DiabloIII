﻿SetBatchLines, -1

Folder = D:\

FileDelete, %Folder%\out.txt

Global Result := ""

ALLFiles(Path, SP, Num) {
	Num := Num + 1
	If Num > 1
	{
		SP .= "  "
	}

	Loop, Files, %Path%\*, DF
	{
		If A_LoopFileAttrib contains H
		{
			Continue
		}

		If A_LoopFileAttrib contains D
		{
			If Num = 1
			{
				Result .= SP "`n--------------------------------------------------`n" A_LoopFileName "`n--------------------------------------------------`n"
			}
			Else If Num = 2
			{
				Result .= "`n" SP "『" A_LoopFileName "』`n"
			}
			Else
			{
				Result .= SP "「" A_LoopFileName "」`n"
			}
			ALLFiles(A_LoopFileLongPath, SP, Num)
		}
		Else
		{
			If A_LoopFileExt in mkv,mp4,avi,ts,m2ts
			{
				Result .= SP "╚═" A_LoopFileName " - (" Round(A_LoopFileSizeMB / 1024, 2) "G)`n"
			}
		}
	}
}

ALLFiles(Folder, "", 0)
FileAppend % Result, %Folder%\out.txt, UTF-8-RAW

Return

ExitApp
