#SingleInstance force ;只能启动一个ahk程序实例，防止重复启动
#Persistent
#NoEnv

;暗黑3/圣教军/阿克汉天谴/AHK脚本
;##################################
;说明：
;-----Macro_State = 0代表宏已停止启动，Macro_State = 1代表步兵宏已启动，Macro_State = 2代表骑兵宏已启动
;-----步兵宏启动后，左键切换天谴延迟800ms，右键切换天谴延迟650ms
;-----骑兵宏启动后，左键停止一切技能上马跑，右键下马开启一切技能砍(同时左键连点拾取)
;-----宏启动后，按住Alt键左键连点拾取
;-----宏启动后，按住Ctrl键取消强制移动，人物原地站立
;-----宏启动后，按键盘H回城并停止宏，再启动按键盘1|2
;默认按键：
;-----鼠标左键 = 阿卡拉特勇士
;-----鼠标右键 = 天谴
;-----键盘A = 挑衅
;-----键盘S = 勇气律法
;-----键盘D = 主要技能/战马冲锋
;-----键盘F = 钢铁之肤
;-----键盘W = 强制移动
;-----键盘1 = 启动/停止步兵宏
;-----键盘2 = 启动/停止骑兵宏
;##################################

;保证宏只在暗黑3中执行
#IfWinActive, ahk_class D3 Main Window Class
	Global Macro_State := 0
	Global Sleep_Delay := 50
	Global BingTunDelay_Slow := 1590
	Global BingTunDelay_Fast := 530
	Global BingTunDelay_SpeedUp := False

	;速刷
	$1::
		If (Macro_State = 0) {
			While GetKeyState("1", "P") {
				Gosub, 冰吞
				Sleep % Sleep_Delay
			}
			If (GetKeyState("CapsLock", "T")) {
				BingTunDelay_Slow := 3333
			} Else {
				BingTunDelay_Slow := 1590
			}
			DiabloIIIMacro(True, True)
		} Else {
			DiabloIIIEndMacro()
		}
	Return

	;按住W效果：左键连点，解决强制移动时拾取不便
    ~W::
        If (Macro_State > 0) {
            While GetKeyState("W", "P") {
                Gosub, 左键
                Sleep % Sleep_Delay
            }
        }
    Return

    ~D Up::
    	If (Macro_State > 0) {
			Gosub, 扫射ON
		}
	Return

	~Space Up::
		If (Macro_State > 0) {
			SetTimer, 冰吞, Off, 999
			If (BingTunDelay_SpeedUp) {
				SetTimer, 冰吞, % BingTunDelay_Slow, 999
				SoundBeep, 300, 300
			} Else {
				SetTimer, 冰吞, % BingTunDelay_Fast, 999
				SoundBeep, 900, 100
				SoundBeep, 900, 100
			}
			BingTunDelay_SpeedUp := !BingTunDelay_SpeedUp
		}
	Return

	~Ctrl::
		If (Macro_State > 0) {
			BingTunDelay_SpeedUp := False
			SetTimer, 冰吞, Off, 999
			Gosub, 扫射OFF
			While GetKeyState("Ctrl", "P") {
				Sleep % Sleep_Delay
			}
			Gosub, 扫射ON
			SetTimer, 冰吞, % BingTunDelay_Slow, 999
		}
	Return

	;大地图、装备、聊天、回城时停止宏
	~B::
	~Q::
	~R::
	~Enter::
		If (Macro_State > 0) {
			;StringReplace, key, A_ThisHotkey, ~, , All
			;GetKeyState(""key, "P")
			DiabloIIIEndMacro()
		}
	Return

	;技能设置：自定义技能按键配置
	左键:
		DiabloIIISendInput("{LButton}")
	Return

	烟幕:
		DiabloIIISendInput("{A}")
	Return

	冰吞:
		DiabloIIISendInput("{S}")
		Sleep 150
		DiabloIIISendInput("{S}")
	Return

	扫射ON:
		DiabloIIISendInput("{D Down}")
	Return

	扫射OFF:
		DiabloIIISendInput("{D Up}")
	Return

	复仇:
		DiabloIIISendInput("{F}")
	Return

	战宠:
		DiabloIIISendInput("+{LButton}")
	Return

	蓄势待发:
		DiabloIIISendInput("{RButton}")
	Return

	药水:
		DiabloIIISendInput("{E}")
	Return

#IfWinActive

;函数：保证按键和点击只在暗黑3中发送
DiabloIIISendInput(Keys) {
    If WinActive("ahk_class D3 Main Window Class") {
		SendInput % Keys
    }
}

;函数：宏启动和停止
DiabloIIIMacro(StartMacro, AutoBlood) {
	If (StartMacro) {
		Gosub, 扫射ON
		Gosub, 复仇
		Gosub, 烟幕
		Gosub, 蓄势待发
	} Else {
		Gosub, 扫射OFF
	}

	SetTimer, 冰吞, % (StartMacro ? BingTunDelay_Slow : "Off"), 999
	SetTimer, 复仇, % (StartMacro ? 280 : "Off"), 99
	SetTimer, 烟幕, % (StartMacro ? 290 : "Off"), 39
	SetTimer, 蓄势待发, % (StartMacro ? 300 : "Off"), 19
	SetTimer, 药水, % (StartMacro and AutoBlood ? 310 : "Off")

	Macro_State := StartMacro ? 1 : 0
	BingTunDelay_SpeedUp := False
}


DiabloIIIEndMacro() {
	Loop, 5 {
		If (Macro_State > 0) {
			DiabloIIIMacro(False, False)
		} Else {
			Break
		}
		Sleep 100
	}
}
