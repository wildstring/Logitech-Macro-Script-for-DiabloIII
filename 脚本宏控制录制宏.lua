EnablePrimaryMouseButtonEvents(true);
EventSource = "MOUSE_BUTTON_PRESSED"; --罗技键盘G_PRESSED

CurrentMacro = ""; --运行中的宏
Macro1 = "TianQian650";
Macro2 = "PaoMa";

--鼠标按键代码
LeftButton = 1;
RightButton = 2;
MiddleButton = 3;
X1Button = 4;
X2Button = 5;

function OnEvent(event, arg, family)
	Macro_Start_Stop(Macro1, X2Button, event, arg); --侧键前进启动宏，再按结束宏
	Macro_Switch(Macro2, LeftButton, event, arg); --左键上马
	Macro_Switch(Macro1, RightButton, event, arg); --右键天谴
end

--启动和结束宏
function Macro_Start_Stop(MacroName, MouseButton, event, arg)
    if (event == EventSource and arg == MouseButton) then
		if (CurrentMacro ~= "") then
			RestartMacro("");
			Macro_Release(); --松开全部未释放的按键
		else
			RestartMacro(MacroName);
        end
    end
end

--切换宏
function Macro_Switch(MacroName, MouseButton, event, arg)
	if (event == EventSource and arg == MouseButton) then
		if (CurrentMacro ~= "") then
			RestartMacro(MacroName);
		end
    end
end

function RestartMacro(MacroName)
    AbortMacro();
    PlayMacro(MacroName);
    CurrentMacro = MacroName;
end

function Macro_Release()
	ReleaseKey("W");
end
