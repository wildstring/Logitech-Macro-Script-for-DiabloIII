#SingleInstance force ;只能启动一个ahk程序实例，防止重复启动
#Persistent
#NoEnv

;暗黑3/魔法师/塔拉夏陨石/AHK脚本
;##################################
;默认按键：
;-----鼠标左键 = 魔法武器
;-----鼠标右键 = 暴风护甲
;-----键盘A = 传送
;-----键盘S = 守护之星
;-----键盘D = 陨石
;-----键盘F = 钻石/黑洞
;-----键盘E = 药水
;-----键盘W = 强制移动
;##################################

;保证宏只在暗黑3中执行
#IfWinActive, ahk_class D3 Main Window Class
	Global Macro_State := 0
	Global Sleep_Delay := 50
	Global Timer_Delay := 300
	Global Potion_Auto := False

	$S::
		DiabloIIIMacro(True)
	Return

	$D::
		Gosub, 钻石黑洞
		Gosub, 陨石
		If (Potion_Auto) {
			Gosub, 药水
		}
	Return

	~Space Up::
		If (Potion_Auto) {
			SoundBeep, 300, 300
		} Else {
			SoundBeep, 900, 100
			SoundBeep, 900, 100
		}
		Potion_Auto := !Potion_Auto
	Return

	;技能设置：自定义技能按键配置
	左键:
		DiabloIIISendInput("{LButton}")
	Return

	守护之星:
		DiabloIIISendInput("{S}")
	Return

	陨石:
		DiabloIIISendInput("{D}")
	Return

	钻石黑洞:
		DiabloIIISendInput("{F}")
	Return

	魔法武器:
		DiabloIIISendInput("+{LButton}")
	Return

	暴风护甲:
		DiabloIIISendInput("{RButton}")
	Return

	药水:
		DiabloIIISendInput("{E}")
	Return

#IfWinActive

;函数：保证按键和点击只在暗黑3中发送
DiabloIIISendInput(Keys) {
    If WinActive("ahk_class D3 Main Window Class") {
		SendInput % Keys
    }
}

;函数：宏启动和停止
DiabloIIIMacro(AutoPotion) {
	Gosub, 守护之星
	Gosub, 魔法武器
	Gosub, 暴风护甲
	Potion_Auto := AutoPotion
}
